#ifndef __FUNCTIONAL_UNIT_H_
#define __FUNCTIONAL_UNIT_H_

#include "memory.h"
#include "type.h"

class FunctionUnit {
	
	public:
		// Constructor
		FunctionUnit(uint32_t neuron_n, Memory<bool>* NBout, Weight threshold);

		// Destructor
		~FunctionUnit();
		
		// Activate Functional unit
		void integrate();
		void fire(Time t);
		void reset();

		// getter
		void* getBuffer() {return fu_buffer;}
		Weight* getPotential() {return poten_accum;}
		uint32_t getFireCount() {return fire_count;}

		// 
		void minus_activation();

		// Dump function
		void dump_buffer();

	private:
		Weight* fu_buffer;
		bool* output_buffer;
		
		Weight* poten_accum; // Potential acummulator

		Memory<bool>* NBout;

		// Neuron parameter;
		Weight threshold;
		uint32_t neuron_n;

		// Computation function
		void update_potential();
		void check_threshold();

		// Measured(?)
		uint32_t add_count;
		uint8_t compare_count;
		uint32_t fire_count;
		


};



#endif /* functional_unit.h */
