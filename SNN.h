#ifndef __SNN_H_
#define __SNN_H_

#include "layer.h"
#include "memory.h"
#include "type.h"
#include "encode.h"
#include "WeightReader.h"

class SnnEmulator {
	public:
		// Constructor
		SnnEmulator(uint32_t hidden_n, uint32_t input_size, uint32_t *neuron_n,
				uint32_t output_size, Time timestep, Weight *thresholds);

		// Destructor
		~SnnEmulator();

		int run();		// Return result of classification
		void reset(); // Prepare to next input
		void clear() {} // Clear all memory and buffers

		void set_input(uint8_t* input);
		void set_input(std::string input);
		void set_weight(WeightReader *reader);

		uint32_t getSpike() {return spike_n;}
		
		void dump_spikeN();

	private:
		//Layer* input;
		Layer** hiddens;
		OutputLayer* output;

		Memory<bool>** connections; // NBin or NBout

		// Parameterss
		uint32_t input_size; // Size of input (ex) MNIST 28*28)
		uint32_t output_size; // Size of output (ex) MNIST 10)
		uint32_t* neurons_n; // # of neurons
		uint32_t hidden_n;

		Time timestep;

		// Measured
		uint32_t spike_n;
};



#endif /* SNN.h */
