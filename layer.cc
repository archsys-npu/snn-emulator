#include "layer.h"
#include <iostream>

Layer::Layer(Memory<bool>* NBin, Memory<bool>* NBout,
					uint32_t neuron_input_n, uint32_t neuron_output_n, Weight threshold)
{
	this->NBin = NBin;
	this->NBout = NBout;

	input_n = neuron_input_n;
	output_n = neuron_output_n;

	SB = new Memory<Weight> (input_n*output_n*sizeof(Weight), input_n, output_n, SRAM);

	//std::cout << "Input: " << input_n << "\tOutput: " << output_n << std::endl;

	decoder = new Decoder(this->NBin, SB);
	fu = new FunctionUnit(output_n, this->NBout, threshold);
}

Layer::~Layer()
{
	delete SB;
	delete decoder;
	delete fu;
}


void Layer::run(Time t)
{
	while(!decoder->decoding(t, fu->getBuffer()))
	{
		fu->integrate();
	}

	fu->fire(t);
	decoder->reset();
	
	spike_n = fu->getFireCount();
}

void Layer::setWeight(WeightReader* reader)
{
	Weight** buffer = new Weight*[input_n];

	for(int i=0; i<input_n; i++)
		buffer[i] = new Weight[output_n];

	for(int o=0; o<output_n; o++)
	{
		for(int i=0; i<input_n; i++)
		{
			//buffer[i][o] = reader->readFloat();
			buffer[i][o] = reader->readDouble();
		}
	}

	for (int i=0; i<input_n; i++)
	{
		SB->mem_store(i, buffer[i], output_n);
		delete buffer[i];
	}
	delete buffer;
}

void Layer::reset()
{
	fu->reset();
	decoder->reset();
}

void OutputLayer::run(Time t)
{
	//fu->ump_buffer();
	while(!decoder->decoding(t, fu->getBuffer()))
		fu->integrate();
	decoder->reset();

	//fu->minus_activation();
}

int OutputLayer::classification()
{
	int idx = -1;
	Weight max = -1000;
	Weight *poten = fu->getPotential();

	//std::cout << "Potential: ";
	for(int i=0; i<output_n; i++)
	{
		if(poten[i] > max)
		{
			max = poten[i];
			idx = i;
		}
		//std::cout << poten[i] << "\t";
	}
	//std::cout << std::endl;

	return idx;
}
