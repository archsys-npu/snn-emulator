#include "SNN.h"
#include <cassert>

SnnEmulator::SnnEmulator(uint32_t hidden_n, uint32_t input_size, 
										uint32_t *neuron_n, uint32_t output_size, Time timestep,
										Weight *thresholds)
{
	assert(hidden_n >=0);
	
	this->hidden_n = hidden_n;
	this->input_size = input_size;
	this->output_size = output_size;

	this->timestep = timestep;

	
	// Now for simple test, we consider only neural network which doesn't contain hidden layer
	//hidden_n = 0;
	
	// Set number of neurons
	neurons_n = new uint32_t[hidden_n + 1];
	for(int i=0; i<hidden_n; i++)
		neurons_n[i] = neuron_n[i];

	// Make input/ouput buffers
	connections = new Memory<bool>*[hidden_n + 2];
	connections[0] = new Memory<bool>(timestep*input_size, timestep, input_size, SRAM); 
	for(int i=1; i<=hidden_n; i++)
	{
		connections[i] = new Memory<bool>(timestep*neuron_n[i-1], timestep, neuron_n[i-1], SRAM);
	}
	connections[hidden_n + 1] = new Memory<bool>(timestep*output_size, timestep, output_size, SRAM);

	// Set layer
	//assert(hidden_n == 0 && "Not Implemented");

	if(hidden_n >0)
	{
		hiddens = new Layer*[hidden_n]; 
		hiddens[0] = new Layer(connections[0], connections[1], input_size, neurons_n[0], thresholds[0]);
		for(int i=1; i<hidden_n; i++)
		{
			hiddens[i] = new Layer(connections[i], connections[i+1], neurons_n[i-1], neurons_n[i], thresholds[i]);
		}
		output = new OutputLayer(connections[hidden_n], connections[hidden_n+1], neurons_n[hidden_n-1], output_size); 
	}
	else
		output = new OutputLayer(connections[0], connections[1], input_size, output_size); 

	// Initialize measurable variables (Ex) nubmer of spikes)
	spike_n = 0;
}

SnnEmulator::~SnnEmulator()
{
	delete neurons_n;

	for(int i=0; i<hidden_n+2; i++)
		delete connections[i];
	
	delete connections;
	delete output;
}

int SnnEmulator::run()
{
	for(int t=0; t<timestep; t++)
	{
		for(int h=0; h<hidden_n; h++)
			hiddens[h]->run(t);
		
		output->run(t);
	}

	return output->classification();
}

void SnnEmulator::reset()
{
	for(int h=0; h<hidden_n; h++)
		hiddens[h]->reset();
	output->reset();
}

void SnnEmulator::set_input(uint8_t *input)
{
	assert(timestep == 8 && "Only 8 timestep is allowed");
	uint8_t encode[9] = {ENCODE8_0, ENCODE8_1, ENCODE8_2, ENCODE8_3, ENCODE8_4, ENCODE8_5, ENCODE8_6, ENCODE8_7, ENCODE8_8}; 

	// Initialize temporal input buffer
	bool** input_buf;
	input_buf = new bool*[timestep];
	for(int i=0; i<timestep; i++)
		input_buf[i] = new bool[input_size];

	// Set output
	for(int i=0; i<input_size; i++)
	{
		uint8_t value = encode[(int)(input[i]*8)/255];
		for(int j=0; j<timestep; j++)
		{
			input_buf[j][i] = (bool)((value >> j) & 1);
			if(input_buf[j][i])
				spike_n++;
		}
	}
	
	for(int i=0; i<timestep; i++)
	{
		connections[0]->mem_store(i, input_buf[i], sizeof(bool) * input_size);	
		delete input_buf[i];
	}
	delete input_buf;
}

void SnnEmulator::set_input(std::string input)
{
	//int size = sizeof(input) / sizeof(char);
	int size = 784;
	uint8_t *int_input = new uint8_t[size];

	for(int i=0; i<size; i++)
		int_input[i] = (uint8_t)input[i];

	set_input(int_input);
	delete int_input;
}

void SnnEmulator::set_weight(WeightReader* reader)
{	
	for(int i=0; i<hidden_n; i++)
		hiddens[i]->setWeight(reader);

	output->setWeight(reader);
}

void SnnEmulator::dump_spikeN()
{
	std::cout << "Number of spikes - Input: " << spike_n << "\t";

	
	for(int i=0; i<hidden_n; i++)
	{
		std::cout << "Hidden" << i+1 << ": " << hiddens[i]->getSpikeN() << "\t";

	}

	std::cout << std::endl;

}
