#include <cxxtest/TestSuite.h>
#include "../SNN.h"
#include "../type.h"
#include "../WeightReader.h"
#include "../LMDB.h"
#include "../snnsim.pb.h"

using namespace std;

class SNNTestSuite : public CxxTest::TestSuite
{
private:
  
public:

	void testsnn()
	{
		uint32_t input_n = 28*28;
		uint32_t output_n = 10;
		
		const uint32_t hidden_layer_n = 2;
		uint32_t neurons_n[hidden_layer_n+1] = {200, 200, 0}; // Why hidden+1? - prevent error when hidden_layer_n = 0
		
		Time timestep = 8;
		Weight thresholds[hidden_layer_n+1] = {1.62, 1.62, 0};

		int test_size = 10000;

		// Simulation
		SnnEmulator* sim = new SnnEmulator(hidden_layer_n, input_n, neurons_n, output_n, timestep, thresholds);

		std::cout << "\nCheckpoint 0" << std::endl;
		
		// set Weight
		WeightReader* reader = new WeightReader("cxxtest/2hidden.weight");
		sim->set_weight(reader);

		LMDB db("cxxtest/mnist_test_lmdb");

		std::cout << "Checkpoint 1" << std::endl;

		int correct = 0;
		for(int test=0; test<test_size; test++)
		{
			int predict;
			int answer;

			sim->reset();
			
			Datum datum;
			datum.ParseFromString( db.getValue() );
			std::string data = datum.data();

			sim->set_input(data);
			predict = sim->run();
			answer = datum.label();

			//std::cout << "predict: " << predict << "\tanswer: " << answer << std::endl;
		
			if(predict == answer)
				correct++;
		}

		std::cout << "Correct: " << correct << "/" << test_size << std::endl;
		//std::cout << "Number of spikes - Input: " << sim->getSpike() * 200 / test_size << std::endl;
		sim->dump_spikeN();

		delete sim;
		delete reader;
	}
};

