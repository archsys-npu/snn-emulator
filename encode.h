#ifndef __ENCODE_H_
#define __ENCODE_H_

// For timestep 8
#define ENCODE8_0 0b00000000
#define ENCODE8_1 0b10000000
#define ENCODE8_2 0b10001000
#define ENCODE8_3 0b10010100
#define ENCODE8_4 0b10101010
#define ENCODE8_5 0b10110110
#define ENCODE8_6 0b11011101
#define ENCODE8_7 0b11110111
#define ENCODE8_8 0b11111111


#endif /* encode.h */
