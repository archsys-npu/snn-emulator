CC=g++
CPPFLAGS =-pthread -std=c++11
LDFLAGS=-lprotobuf -llmdb

PROTOC=protoc
PROTO_DIR=./
PROTO=snnsim.proto
PROTO_SRC=$(patsubst %.proto, %.pb.cc, $(PROTO))


SOURCES=SNN.cc decoder.cc functional_unit.cc layer.cc memory.cc WeightReader.cc LMDB.cc $(PROTO_SRC)
MAIN_SRC=main.cc
OBJECTS=$(patsubst %.cc, %.o, $(SOURCES)) main.o
EXECUTABLE=simulator
OBJECTS_EXCEPT_MAIN=$(patsubst %.cc, %.o, $(SOURCES))
OBJECTS=$(OBJECTS_EXCEPT_MAIN) main.o

TEST_DIR=cxxtest
TEST_SRC=$(addprefix $(TEST_DIR)/, runner.cpp)
TEST_EXE=$(addprefix $(TEST_DIR)/, runner)
TESTS=$(addprefix $(TEST_DIR)/, SNNTest.h)

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

#$(OBJECTS): $(SOURCES) $(MAIN_SRC)
#	$(CC) -c $< $(CPPFLAGS) -o $@

$(PROTO_SRC): $(PROTO)
	$(PROTOC) -I=$(PROTO_DIR) --cpp_out=$(PROTO_DIR) $(PROTO)

test: $(TEST_SRC)
	$(CC) -g $(CPPFLAGS) $(TEST_SRC) $(OBJECTS_EXCEPT_MAIN) -o $(TEST_EXE) $(LDFLAGS)
	$(TEST_EXE)

$(TEST_SRC): $(TESTS)
	cxxtestgen --error-printer -o $(TEST_SRC) $(TESTS)

clean:
	rm -f $(OBJECTS) $(TEST_EXE) $(EXECUTABLE) $(TEST_SRC) $(PROTO_SRC)
