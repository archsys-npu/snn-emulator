#include "WeightReader.h"
#include <cassert>

WeightReader::WeightReader(std::string filename)
{
  file.open( filename, std::ios::binary );
  assert( file.good() );
}

float WeightReader::readFloat()
{
  assert( file.good() );
  //std::cout<<"readFloat()"<<std::endl;
  unsigned char temp[sizeof(float)];
  float ret;
  file.read(reinterpret_cast<char*>(temp), sizeof(float));
  ret = reinterpret_cast<float&>(temp);
  //std::cout<<"float: "<<ret<<std::endl;
  //std::cout<<"endof readFloat()"<<std::endl;
  return ret;
}

double WeightReader::readDouble()
{
  assert( file.good() );
  //std::cout<<"readFloat()"<<std::endl;
  unsigned char temp[sizeof(double)];
  double ret;
  file.read(reinterpret_cast<char*>(temp), sizeof(double));
  ret = reinterpret_cast<double&>(temp);
  //std::cout<<"float: "<<ret<<std::endl;
  //std::cout<<"endof readFloat()"<<std::endl;
  return ret;
}


int WeightReader::readInt()
{
  assert( file.good() );
  //std::cout<<"readFloat()"<<std::endl;
  unsigned char temp[sizeof(float)];
  int ret;
  file.read(reinterpret_cast<char*>(temp), sizeof(float));
  ret = reinterpret_cast<int&>(temp);
  //std::cout<<"float: "<<ret<<std::endl;
  //std::cout<<"endof readFloat()"<<std::endl;
  return ret;
}

