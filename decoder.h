#ifndef _DECODER_H_
#define _DECODER_H_

#include "memory.h"
#include "type.h"

class Decoder
{
	public:
		// Constructor
		Decoder(Memory<bool>* m_NBin, Memory<Weight>* m_SB);

		// Destructor
		~Decoder();

		// Decoding
		bool decoding(Time t, void* FU_buffer); // If decoding is finish, then return true

		// Reset function
		void reset();

	private:
		// Connected buffers	
		Memory<bool>* NBin;
		Memory<Weight>* SB;

		bool* decode_buffer; // Temporarily store spike information at time t
		uint32_t decoder_size;

		// Decoding parameter
		uint32_t buffer_pointer;

		// Deocding function
		void load_NBin(Time t);
};





#endif /* decoder.h */
