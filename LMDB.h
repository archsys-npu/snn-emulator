#ifndef __LMDB_H__
#define __LMDB_H__
#include <iostream>
extern "C" {
#include <lmdb.h>
}
#include <assert.h>
#define MDB_CHECK(mdb_status)\
{\
  assert( mdb_status == MDB_SUCCESS);\
}
class LMDB 
{
  public:
    LMDB(std::string db_file);
    ~LMDB();

    std::string getValue();
    void reset();

  private:
    MDB_env* mdb_env_;
    MDB_dbi mdb_dbi_;

    //cursor
    MDB_txn* mdb_txn_;
    MDB_cursor* mdb_cursor_;
    MDB_val mdb_key_, mdb_value_;
};

#endif
