#ifndef __LAYER_H_
#define __LAYER_H_

#include "type.h"
#include "memory.h"
#include "decoder.h"
#include "functional_unit.h"
#include "WeightReader.h"

class Layer 
{
	public:
		// Constructor
		Layer(Memory<bool>* NBin, Memory<bool>* NBout,
					uint32_t neuron_input_n, uint32_t neuron_output_n,
					Weight threshold);

		// Destructor
		~Layer();

		void run(Time t);
		void setWeight(WeightReader* reader);
		void reset();

		uint32_t getSpikeN() {return spike_n;}

	protected:
		Memory<bool>* NBin;
		Memory<Weight>* SB;
		Memory<bool>* NBout;

		Decoder* decoder;
		FunctionUnit* fu;

		uint32_t input_n;
		uint32_t output_n;

		// Measurable
	private:
		uint32_t spike_n;
};


class OutputLayer : public Layer
{
	public:
		OutputLayer(Memory<bool>* NBin, Memory<bool>* NBout, uint32_t neuron_input_n, uint32_t neuron_output_n) 
			: Layer(NBin, NBout, neuron_input_n, neuron_output_n, 0) {}

		void run(Time t);
		int classification();
};

#endif /* layer.h */
