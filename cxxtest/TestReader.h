#include <cxxtest/TestSuite.h>
#include "../SNN.h"
#include "../type.h"
#include "../WeightReader.h"
#include "../LMDB.h"
#include "../snnsim.pb.h"

using namespace std;

class ReaderTestSuite : public CxxTest::TestSuite
{
private:
  
public:

	void testreader()
	{
		// set Weight
		WeightReader* reader = new WeightReader("cxxtest/1hidden.weight");
		int test_size = 10000;

		for(int i=0; i<test_size; i++)
			std::cout << reader->readFloat() << std::endl;

		delete reader;
	}
};

