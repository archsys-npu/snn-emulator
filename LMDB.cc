#include "LMDB.h"

const size_t LMDB_MAP_SIZE = 1099511627776; //1TB

LMDB::LMDB(std::string db_file)
{
  MDB_CHECK(mdb_env_create(&mdb_env_));
  MDB_CHECK(mdb_env_set_mapsize(mdb_env_, LMDB_MAP_SIZE));
  int flags = 0;
  flags = MDB_RDONLY | MDB_NOTLS;
  int rc = mdb_env_open(mdb_env_, db_file.c_str(), flags, 0664);

  MDB_CHECK(rc);

  std::cout<<"Opened lmdb "<<db_file<<std::endl;


  //Cursor
  MDB_CHECK(mdb_txn_begin(mdb_env_, NULL, MDB_RDONLY, &mdb_txn_));
  MDB_CHECK(mdb_dbi_open(mdb_txn_, NULL, 0, &mdb_dbi_));
  MDB_CHECK(mdb_cursor_open(mdb_txn_, mdb_dbi_, &mdb_cursor_));
  
}
LMDB::~LMDB()
{

}

std::string LMDB::getValue()
{
  int mdb_status = mdb_cursor_get(mdb_cursor_, &mdb_key_, &mdb_value_, MDB_NEXT);

  return std::string(static_cast<const char*>(mdb_value_.mv_data), mdb_value_.mv_size);
}

void LMDB::reset()
{
  mdb_cursor_renew(mdb_txn_, mdb_cursor_);
  //mdb_cursor_put(mdb_cursor_, &mdb_key_, &mdb_value_, MDB_FIRST); 
}
