#include <cxxtest/TestSuite.h>
#include "../SNN.h"
#include "../type.h"
#include "../WeightReader.h"
#include "../LMDB.h"

using namespace std;

class SNNTestSuite : public CxxTest::TestSuite
{
private:
  
public:

	void testsnn()
	{
		uint32_t input_n = 28*28;
		uint32_t output_n = 10;
		
		const uint32_t hidden_layer_n = 0;
		uint32_t neurons_n[hidden_layer_n+1] = {0}; // Why hidden+1? - prevent error when hidden_layer_n = 0
		
		Time timestep = 8;
		Weight thresholds[hidden_layer_n+1] = {0};

		int test_size = 10;

		// Simulation
		SnnEmulator* sim = new SnnEmulator(hidden_layer_n, input_n, neurons_n, output_n, timestep, thresholds);

		// set Weight
		WeightReader* reader = new WeightReader("cxxtest/0hidden.weight");
		sim->set_weight(reader);

		LMDB db("cxxtest/mnist_test_lmdb");

		for(int test=0; test<test_size; test++)
		{
			sim->reset();
			
			Datum datum;
			datum.PraseFromString( db.getValue() );
			std::string data = datum.data();

			sim->set_input(data);
		}

		delete sim;
		delete reader;
	}
};

