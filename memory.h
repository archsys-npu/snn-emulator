#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdint.h>
#include "type.h"

enum MEMORY_TYPE {SRAM, DRAM}; 

template <typename T>
class Memory {
	public:
		// Constructor
		Memory(uint32_t memory_size,
					uint32_t row_size, uint32_t column_size,
					MEMORY_TYPE type);

		// Destructor
		~Memory();
	
		// Memory load and store
		void mem_load(const uint32_t addr, void* target, uint32_t data_size); // Load (addr)th row of memory
		void mem_store(uint32_t addr, const void* data, uint32_t data_size); // Store value in (addr)th row of memory

		// getter
		uint32_t getRowSize() {return row_buffer_size;}

	private:
		T** mem;

		uint32_t row_n, column_n;
		uint32_t row_buffer_size;

		// Umm...
		uint32_t load_n, store_n;
		uint32_t load_bit, store_bit;

};

#endif /* memory.h */
