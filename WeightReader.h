#ifndef __WEIGHT_READER_H__
#define __WEIGHT_READER_H__

#include <iostream>
#include <fstream>

class WeightReader
{
public:
  WeightReader(std::string filename);
  float readFloat();
  double readDouble();
  int readInt();
private:
  std::ifstream file;
  
};

#endif
