#include "decoder.h"
#include <iostream>

Decoder::Decoder(Memory<bool>* m_NBin, Memory<Weight>* m_SB)
{
	this->NBin = m_NBin;
	this->SB = m_SB;

	decoder_size = NBin->getRowSize();

	decode_buffer = new bool[decoder_size];
	buffer_pointer = 0;
}

Decoder::~Decoder()
{
	delete decode_buffer;
}

void Decoder::reset()
{
	buffer_pointer = 0;
}

void Decoder::load_NBin(Time t)
{	
	NBin->mem_load(t, decode_buffer, decoder_size); 
}

bool Decoder::decoding(Time t, void* FU_buffer)
{
	if(buffer_pointer == 0)
		load_NBin(t);

	while(buffer_pointer < decoder_size)
	{
		bool spike = decode_buffer[buffer_pointer];
	
		if(spike)
		{
			SB->mem_load(buffer_pointer, FU_buffer, SB->getRowSize());
			//std::cout << "Decoder - load: " << ((Weight*)FU_buffer)[4] << std::endl;
			buffer_pointer++;
			return false;
		}
		else
		{
			buffer_pointer++;
		}
	}

	return true;
}

