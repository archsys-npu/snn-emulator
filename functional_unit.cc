#include "functional_unit.h"
#include <iostream>

FunctionUnit::FunctionUnit(uint32_t neuron_n, Memory<bool>* NBout, Weight threshold)
{
	this->neuron_n = neuron_n;

	fu_buffer = new Weight[neuron_n];
	output_buffer = new bool[neuron_n];

	poten_accum = new Weight[neuron_n];

	for(int i=0; i<neuron_n; i++)
		poten_accum[i] = 0;

	add_count = 0;
	compare_count = 0;
	fire_count = 0;

	this->threshold = threshold;

	this->NBout = NBout;
}

FunctionUnit::~FunctionUnit()
{
	delete fu_buffer;
	delete output_buffer;
	delete poten_accum;
}

void FunctionUnit::reset()
{
	for(int i=0; i<neuron_n; i++)
		poten_accum[i] = 0;
}


void FunctionUnit::integrate()
{
	update_potential();
	add_count += neuron_n;
}

void FunctionUnit::fire(Time t)
{
	check_threshold();
	NBout->mem_store(t, output_buffer, neuron_n);
}

void FunctionUnit::update_potential()
{
	for(int i=0; i<neuron_n; i++)
	{
		poten_accum[i] += fu_buffer[i];	
	}
}

void FunctionUnit::dump_buffer()
{
	std::cout << "Function Unit Buffer" << std::endl;
	for(int i=0; i<neuron_n; i++)
		std::cout << fu_buffer[i] << "\t";
	std::cout << std::endl << std::endl;
}

void FunctionUnit::minus_activation()
{
	for(int i=0; i<neuron_n; i++)
	{
		if(poten_accum[i] < 0)
		{
			poten_accum[i] = 0;
		}
	}
}

void FunctionUnit::check_threshold()
{
	for(int i=0; i<neuron_n; i++)
	{
		if(poten_accum[i] >= threshold)
		{
			output_buffer[i] = true;
			poten_accum[i] = 0;
			fire_count++;
		}
		else
			output_buffer[i] = false;
	
		minus_activation();
	}
}
