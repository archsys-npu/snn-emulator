#include "memory.h"
#include <cassert>
#include <cstring>
#include <iostream>

template <typename T>
Memory<T>::Memory(uint32_t memory_size, 
							uint32_t row_n, uint32_t column_n, 
							MEMORY_TYPE type)
{	
	assert(memory_size == row_n * column_n * sizeof(T));

	this->row_n = row_n;
	this->column_n = column_n;

	//row_buffer_size = column_n / sizeof(T);
	row_buffer_size = column_n;

	mem = new T*[row_n];
	for(int i=0; i<row_n; i++)
	{
		mem[i] = new T[column_n];
	}

	// Initailize values
	load_n = 0;
	store_n = 0;
	load_bit = 0;
	store_bit = 0;
}

template <typename T>
Memory<T>::~Memory()
{
	for(int i=0; i<row_n; i++)
		delete mem[i];
	delete mem;
}

template <typename T>
void Memory<T>::mem_load(const uint32_t addr, void* target, uint32_t data_size)
{
	assert(addr < row_n);
	assert(data_size == row_buffer_size);

	load_n++;
	load_bit += row_buffer_size;

	memcpy (target, mem[addr], data_size * sizeof(T));
	
	//return mem[addr];	
}

template <typename T>
void Memory<T>::mem_store(uint32_t addr, const void* data, uint32_t data_size)
{
	assert(addr < row_n);
	//std::cout << "data_size : " << data_size << "\trow_size: " << row_buffer_size << std::endl;
	assert(data_size == row_buffer_size);
	
	store_n++;
	store_bit += row_buffer_size;

	memcpy (mem[addr], data, data_size * sizeof(T));

	return;
}

template class Memory<bool>;
template class Memory<Weight>;

